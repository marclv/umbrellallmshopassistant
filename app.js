var express = require('express'); // to easily have a web server
var app = express();

//app.use(express.urlencoded({ extended: true })); // to be able to read the POST data from an html form. Need to reactivate when I have filter for form also
app.use(express.text(), express.json()); // override the line before, to be able to understand "text/plain" from the fetch call in the frontends, see https://stackoverflow.com/questions/12345166

const path = require('path');
const myPrettyDatabase = require('./database/database');
const { UserQuery } = require('./classes/userQuery');
const e = require('express');
const publicDir = path.join(__dirname, 'public');

myPrettyDatabase.initDatabase();

app.use(express.static('public'));

app.get('/', function (req, res) {
   res.sendFile(path.join(publicDir, '/index.html'));
});

var server = app.listen(5000, function () {
   console.log('Express App running at http://127.0.0.1:5000/');
});

const getCatPost = (num) => {
   return fetch('https://catfact.ninja/fact')
       .then(response => response.json());
};

const getCompletions = (userQuery) => {
   console.log("userQuery", userQuery);
   const fullPrompt = `The following is a JSON extracted from the user query according to this informations :
   orderResultBy: a string indicating how to order the results, can only be  "price descending", "price ascending", "speedOfDeployment descending", "speedOfDeployment ascending", "canopySize descending", "canopySize ascending" or null.
   brands: an array of string indicating the umbrella brand names wanted by the user. Leave null if the user didn't specify any brand. Do not mix brand names with colors. Leave null if you're not 100% sure it's a brand name.
   maxPrice: a number, maximum price asked by the user in euros. Do not try to deduce a price if the user didn't explicitly gave a number.
   minimumPrice: a number, minimum price asked by the user in euros.  Do not try to deduce a price if the user didn't explicitly gave a number.
   approximatePrice: a string that can be only "cheap", "moderate", "expensive" or null. Leave null if you're not 100% sure the user is specifying a number. Be careful, do not mix it with how the user wants to order the results (for instance "lower price first" do not mean the user want a cheap umbrella).
   canopyColors: an array of string representating the colors of the canopy wanted by the user. It's considered to be the default color if the user doesn't specify he's talking about handle color then it's canopy color. Leave it null if no color is specified by the user.
   handleColors: an array of string representating the colors of the handle wanted by the user. This is not the main color. It's alimented only if the user explicitly mention the color the handle should have.  Leave it null if no color is specified by the user.
   approximateRobustness: a string, can only be  "fragile", "durable", "indestructible" or null.
   customCanopyLogo: a boolean that can be null, if the user need to have his logo printed on the canopy (true), don't need it (false) or don't mention it (null).
   approximateCanopySize: a string, can only be  "small", "medium", "large" (for 2 people), "very large" (for 3 people) or null.
   maxCanopySize: a number, the maximum canopy size required by the user, in cm. Leave null if the user didn't explicitly gave a number.
   minCanopySize: a number, the minimum canopy size required by the user, in cm. Leave null if the user didn't explicitly gave a number.
   approximateSpeedOfDeployment: a string, can only be "slow", "average speed", "fast" or null. Leave null if unspecified.
   speedOfDeployment: a number, indicating how fast the umbrella need to be openable in seconds.
   automatedDeployment: a boolean that can be null, indicating if the user wants an umbrella that deploy easily with the push of a button (true), if he wants to open it manually (false) or if the user doesn't specify it (null).
   
   This is the user query:
   ${userQuery}
   This is the JSON summarizing the user needs, field that cannot be deduced are left as null: 
                              {
                                 "`.replace(/(?:\r\n|\r|\n)/g, '\\n').replace(/"/g, '\\"');;
   const options = {
      method: 'POST',
      url: 'http://localhost:5001/api/v1/post_v1_completions',
      body: `{ "prompt": "${fullPrompt}", "max_tokens": 200,"temperature": 0}`,
  };

   return fetch('http://localhost:5001/api/v1/completions', options)
       .then(response => response.json());
};

app.post('/generateQueryFromNaturalLanguage', async function (req, res) {
   try{
      const data = await getCompletions(req.body);
      const jsonFromLlm = parseJsonFromLLm('{"'+data.choices[0].text, res);
      res.send(jsonFromLlm);
   } catch(err) {
      console.log(err);
   }
});

app.post('/postMock', async function (req, res) {
      const userQuery = new UserQuery(req.body);
       return await myPrettyDatabase.query(userQuery, function(err, retour) {
         if (err) {
           console.error("Failed to get umbrellas", err);
           return;
         }
         res.send(retour);
       });
});

function parseJsonFromLLm(txt, res) {
   try {
      console.log("raw from llM", txt);
      console.log("cutted from raw from llM", txt.replace(/\}.*/, '}'));
      console.log("cutted from raw from llM variante", txt.substring(0, txt.indexOf("}")));
      txt = str.substring(txt.substring(0, txt.indexOf("}"))+"}"); // keeps only what's before "}" in the LLM answer.
      txt = str.substring(str.indexOf("{")); // keeps only what's after the first "{" in the LLM answer. Shouldn't have nothing since we preprompted { but...
      const userQuery = JSON.parse(txt);
       console.log("once cutted",userQuery);
       return userQuery;
     } catch (error) {
      console.log(error);
      res.status(500).send(`The LLM didn't return a JSON.`);
     }
}