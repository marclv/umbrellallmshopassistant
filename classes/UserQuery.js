class UserQuery {
    constructor(obj) {
      this.brands = obj.brands;
      this.maxPrice = obj.maxPrice;
      this.minimumPrice = obj.minimumPrice;
      this.approximatePrice = obj.approximatePrice;
      this.canopyColors = obj.canopyColors;
      this.handleColors = obj.handleColors;
      this.approximateRobustness = obj.approximateRobustness;
      this.customCanopyLogo = obj.customCanopyLogo;
      this.approximateCanopySize = obj.approximateCanopySize;
      this.minCanopySize = obj.minCanopySize;
      this.maxCanopySize = obj.maxCanopySize;
      this.approximateSpeedOfDeployment = obj.approximateSpeedOfDeployment;
      this.speedOfDeployment = obj.speedOfDeployment;
      this.automatedDeployment = obj.automatedDeployment;
      this.orderResultBy = obj.orderResultBy;
    }
  }

  module.exports = { UserQuery };
