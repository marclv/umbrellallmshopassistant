const { UserQuery } = require('../classes/userQuery');
var arrayOfUmbrellas = require('./arrayOfUmbrellas');

var Engine = require('tingodb')();
var db = new Engine.Db('./database', {});

function initDatabase() {

   db.collection("umbrellas").drop(function(err, delOK) {
      if (err) throw err;
      if (delOK) console.log("Collection deleted.");
      db.collection("umbrellas").insert(arrayOfUmbrellas.umbrellas, function(err, res) {
         if (err) throw err;
         if (delOK) console.log("Collection recreated.");
         console.log("Number of umbrellas inserted: " + res.length);

         //Just to check if everything is working properly
         /*
         var query = { brand: /daisy/i };
         db.collection("umbrellas").find(query).toArray(function(err, result) {
           if (err) throw err;
           console.log(result);
           db.close();
         });
         */
       });
    });
}

async function query(userQuery, callback) {
   if (!userQuery instanceof UserQuery) { throw new Error('Cannot query without an UserQuery!'); }
   console.log("need to query the db with", userQuery);
   if (userQuery.approximatePrice && userQuery.approximatePrice.toLowerCase() === 'cheap' && userQuery.maxPrice == undefined) {
      userQuery.maxPrice = 20;
   }
   if (userQuery.approximatePrice && userQuery.approximatePrice.toLowerCase() === 'moderate' && userQuery.maxPrice == undefined) {
      userQuery.maxPrice = 80;
   }

   if (userQuery.approximateCanopySize && userQuery.approximateCanopySize.toLowerCase() === 'small' && userQuery.maxCanopySize == undefined) {
      userQuery.maxCanopySize = 90;
   }

   const conditions = [
      userQuery.brands && userQuery.brands.length > 0 ? { brand: { $in: userQuery.brands } } : null,
      userQuery.maxPrice != null ? { price: { $lte: userQuery.maxPrice } } : null,
      userQuery.minimumPrice != null ? { price: { $gte: userQuery.minimumPrice } } : null,
      userQuery.canopyColors && userQuery.canopyColors.length > 0 ? { 'canopy.color': { $in: userQuery.canopyColors } } : null,
      userQuery.customCanopyLogo != null ? { 'canopy.customLogoAvailable': userQuery.customCanopyLogo } : null,
      userQuery.maxCanopySize != null ? { canopySize: { $lte: userQuery.maxCanopySize } } : null,
      userQuery.minCanopySize != null ? { canopySize: { $gte: userQuery.minCanopySize } } : null,
      userQuery.handleColors && userQuery.handleColors.length > 0 ? { handleColor: { $in: userQuery.handleColors } } : null,
      userQuery.approximateRobustness != null ? { robustness: userQuery.approximateRobustness } : null,
      userQuery.customCanopyLogo != null ? { customCanopyLogo: userQuery.customCanopyLogo } : null,
      userQuery.speedOfDeployment != null ? { 'deploymentInfos.speedOfDeployment': userQuery.speedOfDeployment } : null,
      userQuery.automatedDeployment != null ? { 'deploymentInfos.automatedDeployment': userQuery.automatedDeployment } : null
   ].filter(condition => condition !== null);

   console.log("===============conditions==============");
   console.log(conditions);

   const query = { $or: conditions };
   /*
   const sort = userQuery.orderResultBy ? { [userQuery.orderResultBy]: 1 } : {};

   const mongoQuery = {
   query,
   sort
   };
   console.log("===============QUERY==============");
   console.log(query);
   */

   const aggregationPipeline = [
      { $match: query },
      {
        $addFields: {
          matchCount: {
            $add: conditions.map(condition => ({
              $cond: [{ $and: Object.entries(condition).map(([key, value]) => ({ $eq: [`$${key}`, value] })) }, 1, 0]
            }))
          }
        }
      },
      { $sort: { matchCount: -1 } },
      { $project: { matchCount: 0 } }  // Optionally, hide the matchCount field from the output
    ];


   db.collection("umbrellas").find(query).toArray(function(err, docs) {
      if (err) {
        db.close();
        return callback(err, null);
      }
      db.close();

  // Debugging: Log the initial documents and conditions
  console.log("Initial Documents:", docs);
  console.log("Conditions:", JSON.stringify(conditions, null, 2));


  // Step 2: Count the number of matching conditions for each document
  docs.forEach(doc => {
   doc.matchCount = conditions.reduce((count, condition) => {
     const matchesCondition = Object.keys(condition).every(key => {
       const value = condition[key];
       const docValue = getNestedValue(doc, key);
       
       // Debugging: Log the evaluation of each condition
       console.log(`Evaluating condition: ${JSON.stringify(condition)} on document: ${JSON.stringify(doc)}`);
       console.log(`Document value for ${key}:`, docValue);

       if (value.$in) {
         const result = value.$in.includes(docValue);
         console.log(`$in check: ${result}`);
         return result;
       } else if (value.$lte !== undefined) {
         const result = docValue <= value.$lte;
         console.log(`$lte check: ${result}`);
         return result;
       } else if (value.$gte !== undefined) {
         const result = docValue >= value.$gte;
         console.log(`$gte check: ${result}`);
         return result;
       } else {
         const result = docValue === value;
         console.log(`Equality check: ${result}`);
         return result;
       }
     });
     return count + (matchesCondition ? 1 : 0);
   }, 0);

   // Debugging: Log the match count for each document
   console.log(`Document: ${JSON.stringify(doc)} matches ${doc.matchCount} conditions`);
 });


 // Step 3: Sort the documents by matchCount in descending order
 docs.sort((a, b) => b.matchCount - a.matchCount);

 const biggestMatch = docs[0].matchCount;

 docs.filter(a => a.matchCount<biggestMatch);

 console.log(docs);

      return callback(null, docs);
    });

}

function getNestedValue(obj, path) {
   return path.split('.').reduce((o, k) => (o && o[k] !== 'undefined' ? o[k] : null), obj);
 }

module.exports = { initDatabase, query };