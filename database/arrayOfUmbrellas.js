
var umbrellas = 
[
   {
      _id: 1,
      model: "mb45b",
      brand: "maxbrella",
      image: "MB45B.jpg",
      price: 99.99,
      canopy: {
       color: "black",
       customLogoAvailable: false,
       canopySize: 120,
      },
      handleColor: "black",
      foldable: false,
      robustness: 56,
      deploymentInfos: {    
       speedOfDeployment: 1.2,
       automatedDeployment: true
      },
      desc:""
    },
    {
      _id: 2,
      model: "mb45r",
      brand: "maxbrella",
      image: "MB45R.jpg",
      price: 99.99,
      canopy: {
       color: "red",
       customLogoAvailable: false,
       canopySize: 120,
      },
      handleColor: "black",
      foldable: false,
      robustness: 56,
      deploymentInfos: {    
       speedOfDeployment: 2.2,
       automatedDeployment: true
      },
      desc:""
    },
    {
      _id: 3,
      model: "protekt rev2",
      brand: "protekt",
      image: "ProtektRev2.jpg",
      price: 700.59,
      canopy: {
       color: "black",
       customLogoAvailable: true,
       canopySize: 120,
      },
      handleColor: "black",
      foldable: false,
      robustness: 100,
      deploymentInfos: {    
       speedOfDeployment: 1.8,
       automatedDeployment: true
      },
      desc:""
    },
    {
      _id: 4,
      model: "daisy white",
      brand: "daisy",
      image: "DaisyWhite.jpg",
      price: 12,
      canopy: {
       color: "white",
       customLogoAvailable: false,
       canopySize: 90,
      },
      handleColor: "white",
      foldable: true,
      robustness: 20,
      deploymentInfos: {    
       speedOfDeployment: 1.1,
       automatedDeployment: false
      },
      desc:""
    },
    {
      _id: 5,
      model: "daisy red",
      brand: "daisy",
      image: "DaisyRed.jpg",
      price: 12,
      canopy: {
       color: "red",
       customLogoAvailable: false,
       canopySize: 90,
      },
      handleColor: "white",
      foldable: true,
      robustness: 20,
      deploymentInfos: {    
       speedOfDeployment: 1.1,
       automatedDeployment: false
      },
      desc:""
    },
];

module.exports = { umbrellas };
