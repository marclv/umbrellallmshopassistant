{
  "brand": "string",
  "price": "number",
  "canopy": {
	"color": {  "type": "array",  "items": {    "type": "number"  }},
	"customLogoAvailable": false,
	"canopySize": 90,
  }
  "handleColor": ["blue", "red"],
  "foldable": false
  "robustness": 56,
  "deploymentInfos": {    
	"speedOfDeployment": "1.2s",
	"automatedDeployment": true
  }
}


{
    "brands": null,
    "maxPrice": null,
    "minimumPrice": null,
    "approximatePrice": null,
    "canopyColors": null,
    "handleColors": null,
    "approximateRobustness": null,
    "customCanopyLogo": null,
    "approximateCanopySize": null,
    "exactCanopySize": null,
	"foldable": null,
	"approximateSpeedOfDeployment": null,
	"speedOfDeployment": null,
    "automatedDeployment": null
	"orderResultBy": null
}

====================
brand: a string representating the brand name of the umbrella.
price: a number, price of the umbrella in dollars.
color: an array of string representating the colors of the canopy avalaible.handleColor": ["black"],
canPrintLogo: a boolean, can you ask the manufacturer to print your logo on the umbrella before sending it to you (true) or not (false).
robustness: a number between 0 (very fragile) and 100 (very sturdy)
size: a number indicating the size of the canopy in centimeters
deploymentInfos: a parent nodes with two sub-nodes.
speedOfDeployment: a number, child 1 of deploymentInfos, indicating how fast the umbrella can be opened in seconds.
automatedDeployment: a boolean, child 2 of deploymentInfos, indicating if the umbrella deploy with the push of a button (true) or if you need to open it manually (false).

====================

{
  "brand": "Mini-umbrella",
  "price": "4.2",
  "color": ["blue", "red"],
  "handleColor": ["black"],
  "canPrintLogo": false,
  "robustness": 56,
  "size": 90,
  "deploymentInfos": {    
	"speedOfDeployment": "1.2s",
	"automatedDeployment": true
  }
}

====================

{{[INPUT]}}
Extract in JSON format the following infos from the user query:
brand color canPrintLogo

Left a blank String if you don't have the information. Only output the json, nothing else.

User query:
Hi I'm looking for an Protekt umbrella. I don't care about the color.
{{[OUTPUT]}}
{
"brand": "Protekt",
"color": "",
"canPrintLogo": ""
}
====================
Extract a JSON from the user query according to this informations :
brand: a string representating the brand name of the umbrella.
price: a number, price of the umbrella in euros.
color: an array of string representating the colors of the canopy avalaible.handleColor": ["black"],
canPrintLogo: a boolean, can you ask the manufacturer to print your logo on the umbrella before sending it to you (true) or not (false).
robustness: a number between 0 (very fragile) and 100 (very sturdy)
exactSize: a number indicating the size of the canopy in centimeters
deploymentInfos: a parent nodes with two sub-nodes.
speedOfDeployment: a number, child 1 of deploymentInfos, indicating how fast the umbrella can be opened in seconds.
automatedDeployment: a boolean, child 2 of deploymentInfos, indicating if the umbrella deploy with the push of a button (true) or if you need to open it manually (false).
=========================
Extract a JSON from the user query according to this informations :
brand: a string representating the brand name of the umbrella wanted by the user.
maxPrice: a number, maximum price asked by the user in euros.
minimumPrice: a number, minimum price asked by the user in euros.
approximatePrice: a string, can be only "cheap", "moderate", "expansive" or an empty String.
canopyColor: an array of string representating the colors of the canopy wanted by the user. It's considered to be the default color if the user doesn't specify he's talking about handle color then it's canopy color.
handleColor: an array of string representating the colors of the handle wanted by the user. This is not the main color.
needPrintLogo: a boolean, if the user need to have his logo printed on the canopy or not.
robustness: a number between 0 (very fragile) and 100 (very sturdy).
approximateSize: a string, can only be "very small", "small", "medium", "large", "very large" or an empty String.
exactSize: a number, the exact size required by the user, in cm.
deploymentInfos: a parent nodes with 3 sub-nodes.
approximateSpeedOfDeployment: a string, can only be "very small", "small", "medium", "large", "very large" or an empty String.
speedOfDeployment: a number, child 1 of deploymentInfos, indicating how fast the umbrella need to be openable in seconds.
automatedDeployment: a boolean, child 2 of deploymentInfos, indicating if the umbrella deploy with the push of a button (true) or if you need to open it manually (false).

Leave a blank String if you don't have the information. Only output the json, nothing else.

This is the user query:
Hi, I'm looking for a big Protekt umbrella that can be easily opened.


===========================


===========
```java
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Map<String, Object> userQuery = new HashMap<>();
        // ...

        // Fill userQuery map
        userQuery.put("brand", "Protekt");
        userQuery.put("approximatePrice", "");
        userQuery.put("canopyColor", new ArrayList<>());
        userQuery.put("handleColor", new ArrayList<>());
        userQuery.put("needPrintLogo", false);
        userQuery.put("deploymentInfos", new HashMap<>());

        // Deployment Infos
        ((HashMap<String, Object>) userQuery.get("deploymentInfos")).put("approximateSpeedOfDeployment", "");
        ((HashMap<String, Object>) userQuery.get("deploymentInfos")).put("speedOfDeployment", null);
        ((HashMap<String, Object>) userQuery.get("deploymentInfos")).put("automatedDeployment", true);

        System.out.println(userQuery);
    }
}
```
===============

The following is a JSON extracted from the user query according to this informations :
brand: a string representating the brand name of the umbrella wanted by the user.
maxPrice: a number, maximum price asked by the user in euros.
minimumPrice: a number, minimum price asked by the user in euros.
approximatePrice: a string, can be only "cheap", "moderate", "expansive" or an empty String.
canopyColor: an array of string representating the colors of the canopy wanted by the user. It's considered to be the default color if the user doesn't specify he's talking about handle color then it's canopy color.
handleColor: an array of string representating the colors of the handle wanted by the user. This is not the main color.
needPrintLogo: a boolean, if the user need to have his logo printed on the canopy or not.
robustness: a number between 0 (very fragile) and 100 (very sturdy).
approximateSize: a string, can only be "very small", "small", "medium", "large", "very large" or an empty String.
exactSize: a number, the exact size required by the user, in cm.
deploymentInfos: a parent nodes with 3 sub-nodes.
approximateSpeedOfDeployment: a string, can only be "very small", "small", "medium", "large", "very large" or an empty String.
speedOfDeployment: a number, child 1 of deploymentInfos, indicating how fast the umbrella need to be openable in seconds.
automatedDeployment: a boolean, child 2 of deploymentInfos, indicating if the umbrella deploy with the push of a button (true) or if you need to open it manually (false).

This is the user query:
Hi, I'm looking for a big Protekt umbrella that can be easily opened.

This is the JSON: 
{
    "brand": "Protekt",
    "maximumPrice": null,
    "minimumPrice": null,
    "approximatePrice": "",
    "canopyColor": [],
    "handleColor": [],
    "needPrintLogo": false,
    "robustness": null,
    "approximateSize": "large",
    "exactSize": null,
    "deploymentInfos": {
        "approximateSpeedOfDeployment": "",
        "speedOfDeployment": null,
        "automatedDeployment": true
    }
}

================
The following is a JSON extracted from the user query according to this informations :
brand: a string representating the brand name of the umbrella wanted by the user.
maxPrice: a number, maximum price asked by the user in euros.
minimumPrice: a number, minimum price asked by the user in euros.
approximatePrice: a string, can be only "cheap", "moderate", "expansive" or an empty String.
canopyColor: an array of string representating the colors of the canopy wanted by the user. It's considered to be the default color if the user doesn't specify he's talking about handle color then it's canopy color.
handleColor: an array of string representating the colors of the handle wanted by the user. This is not the main color.
needPrintLogo: a boolean, if the user need to have his logo printed on the canopy or not.
robustness: a number between 0 (very fragile) and 100 (very sturdy).
approximateSize: a string, can only be "very small", "small", "medium", "large", "very large" or "".
exactSize: a number, the exact size required by the user, in cm.
deploymentInfos: a parent nodes with 3 sub-nodes.
approximateSpeedOfDeployment: a string, child 1 of deploymentInfos, can only be "slow", "average speed", "fast" or "".
speedOfDeployment: a number, child 2 of deploymentInfos, indicating how fast the umbrella need to be openable in seconds.
automatedDeployment: a boolean, child 3 of deploymentInfos, indicating if the umbrella deploy with the push of a button (true) or if you need to open it manually (false).

This is the user query:
Hi, I'm looking for an umbrella that can protect me, my girlfriend and my child together from the rain. The color must evoke love or passion. Less than 50 euros please.

This is the JSON summarizing the user needs, field that cannot be deduced are left empty/null: 
{
    "brand": null,
    "maxPrice": null,
    "minimumPrice": null,
    "approximatePrice": "",
    "canopyColor": ["red", "pink"],
    "handleColor": [],
    "needPrintLogo": false,
    "robustness": null,
    "approximateSize": "large",
    "exactSize": null,
    "deploymentInfos": {
        "approximateSpeedOfDeployment": "",
        "speedOfDeployment": null,
        "automatedDeployment": false
    }
}

The user wants an umbrella for him, his girlfriend and his child. He didn't specify any brand preference, maximum or minimum price, approximate price level, handle color, need for logo printing, robustness, exact size or deployment speed. However, he did mention that the color should evoke love or passion, so we set the canopy color to red and pink. We also assume that the approximate size is large to accommodate the whole family under it. The deployment info is left empty as the user didn't provide any information regarding the deployment speed or automation.

==============
================
The following is a JSON extracted from the user query according to this informations :
brand: a string representating the brand name of the umbrella wanted by the user.
maxPrice: a number, maximum price asked by the user in euros. Do not try to deduce a price if the user didn't explicitly gave a number.
minimumPrice: a number, minimum price asked by the user in euros.  Do not try to deduce a price if the user didn't explicitly gave a number.
approximatePrice: a string that can be only "cheap", "moderate", "expansive" or null. In this field you can try to deduce the range.
canopyColors: an array of string representating the colors of the canopy wanted by the user. It's considered to be the default color if the user doesn't specify he's talking about handle color then it's canopy color. Leave it null if no color is specified by the user.
handleColors: an array of string representating the colors of the handle wanted by the user. This is not the main color. It's alimented only if the user explicitly mention the color the handle should have.  Leave it null if no color is specified by the user.
needPrintLogo: a boolean that can be null, if the user need to have his logo printed on the canopy (true), don't need it (false) or don't mention it (null).
robustness: a number between 0 (very fragile) and 100 (very sturdy).
approximateSize: a string, can only be  "small", "medium", "large" (for 2 people), "very large" (for 3 people) or null.
exactSize: a number, the exact size required by the user, in cm.
deploymentInfos: a parent nodes with 3 sub-nodes.
approximateSpeedOfDeployment: a string, child 1 of deploymentInfos, can only be "slow", "average speed", "fast" or null.
speedOfDeployment: a number, child 2 of deploymentInfos, indicating how fast the umbrella need to be openable in seconds.
automatedDeployment: a boolean that can be null, child 3 of deploymentInfos, indicating if the user wants an umbrella that deploy easily with the push of a button (true), if he wants to open it manually (false) or if the user doesn't specify it (null).

This is the user query:
Hi, I want a very sturdy umbrella that I can open very easily because I'm a bit clumsy. Less than 20 euros please.

This is the JSON summarizing the user needs, field that cannot be deduced are left as null: 
{
    "brand": null,
    "maxPrice": 20,
    "minimumPrice": null,
    "approximatePrice": "cheap",
    "canopyColors": null,
    "handleColors": null,
    "needPrintLogo": null,
    "robustness": 100,
    "approximateSize": null,
    "exactSize": null,
    "deploymentInfos": {
        "approximateSpeedOfDeployment": "fast",
        "speedOfDeployment": null,
        "automatedDeployment": true
    }
}

============
The following is a JSON extracted from the user query according to this informations :
brand: a string representating the brand name of the umbrella wanted by the user.
maxPrice: a number, maximum price asked by the user in euros.
minimumPrice: a number, minimum price asked by the user in euros.
approximatePrice: a string, can be only "cheap", "moderate", "expansive" or an empty String.
canopyColor: an array of string representating the colors of the canopy wanted by the user. It's considered to be the default color if the user doesn't specify he's talking about handle color then it's canopy color.
handleColor: an array of string representating the colors of the handle wanted by the user. This is not the main color.
needPrintLogo: a boolean, if the user need to have his logo printed on the canopy or not.
robustness: a number between 0 (very fragile) and 100 (very sturdy).
approximateSize: a string, can only be "very small", "small", "medium", "large", "very large" or "".
exactSize: a number, the exact size required by the user, in cm.
deploymentInfos: a parent nodes with 3 sub-nodes.
approximateSpeedOfDeployment: a string, child 1 of deploymentInfos, can only be "slow", "average speed", "fast" or "".
speedOfDeployment: a number, child 2 of deploymentInfos, indicating how fast the umbrella need to be openable in seconds.
automatedDeployment: a boolean, child 3 of deploymentInfos, indicating if the umbrella deploy with the push of a button (true) or if you need to open it manually (false).

This is the user query:
very sturdy umbrella 70-120 euros range

This is the JSON summarizing the user needs, field that cannot be deduced are left empty/null: 
{
    "brand": "",
    "maxPrice": 120,
    "minimumPrice": 70,
    "approximatePrice": "",
    "canopyColor": [],
    "handleColor": [],
    "needPrintLogo": false,
    "robustness": 100,
    "approximateSize": "",
    "exactSize": null,
    "deploymentInfos": {
        "approximateSpeedOfDeployment": "",
        "speedOfDeployment": null,
        "automatedDeployment": false
    }
}

The user query is asking for a very sturdy umbrella within the price range of 70 to 120 euros. All other fields are left empty as they were not specified in the query. The robustness is set to 100 since the user explicitly asked for a very sturdy umbrella. The approximatePrice is also left empty as the user didn't specify any preference regarding the price category. The canopyColor, handleColor, needPrintLogo, approximateSize, exactSize, deploymentInfos fields are all left empty as they were not mentioned in the query.

Here's an example of a more complete user query and its corresponding JSON representation:

User Query: I want a red and black striped umbrella with a logo printed on it, that deploys automatically in under 5 seconds, costs around 50 euros and has a medium size canopy.

JSON representation:
{
    "brand": "",
    "maxPrice": 50,
    "minimumPrice": 50,
    "approximatePrice": "moderate",
    "canopyColor": ["red", "black"],
    "handleColor": [],
    "needPrintLogo": true,
    "robustness": null, // not specified in the query
    "approximateSize": "medium",
    "exactSize": null,
    "deploymentInfos": {
        "approximateSpeedOfDeployment": "fast",
        "speedOfDeployment": 5,
        "automatedDeployment": true
    }
}


==========================
The following is a JSON extracted from the user query according to this informations :
brand: a string representating the brand name of the umbrella wanted by the user.
maxPrice: a number, maximum price asked by the user in euros. Do not try to deduce a price if the user didn't explicitly gave a number.
minimumPrice: a number, minimum price asked by the user in euros.  Do not try to deduce a price if the user didn't explicitly gave a number.
approximatePrice: a string that can be only "cheap", "moderate", "expansive" or null. In this field you can try to deduce the range.
canopyColors: an array of string representating the colors of the canopy wanted by the user. It's considered to be the default color if the user doesn't specify he's talking about handle color then it's canopy color. Leave it null if no color is specified by the user.
handleColors: an array of string representating the colors of the handle wanted by the user. This is not the main color. It's alimented only if the user explicitly mention the color the handle should have.  Leave it null if no color is specified by the user.
needPrintLogo: a boolean that can be null, if the user need to have his logo printed on the canopy (true), don't need it (false) or don't mention it (null).
robustness: a number between 0 (very fragile) and 100 (very sturdy).
approximateSize: a string, can only be  "small", "medium", "large" (for 2 people), "very large" (for 3 people) or null.
exactSize: a number, the exact size required by the user, in cm.
deploymentInfos: a parent nodes with 3 sub-nodes.
approximateSpeedOfDeployment: a string, child 1 of deploymentInfos, can only be "slow", "average speed", "fast" or null.
speedOfDeployment: a number, child 2 of deploymentInfos, indicating how fast the umbrella need to be openable in seconds.
automatedDeployment: a boolean that can be null, child 3 of deploymentInfos, indicating if the user wants an umbrella that deploy easily with the push of a button (true), if he wants to open it manually (false) or if the user doesn't specify it (null).

This is the user query:
Hi, I'm looking for an umbrella that can protect me, my girlfriend and my child together from the rain. The color must evoke love or passion.  Less expensive than a croissant please. Also, I want my brand name on it.

This is the JSON summarizing the user needs, field that cannot be deduced are left as null: 
{
    "brand": null,
    "maxPrice": null,
    "minimumPrice": null,
    "approximatePrice": "cheap",
    "canopyColors": ["red", "pink"],
    "handleColors": null,
    "needPrintLogo": true,
    "robustness": null,
    "approximateSize": "very large",
    "exactSize": null,
    "deploymentInfos": {
        "approximateSpeedOfDeployment": null,
        "speedOfDeployment": null,
        "automatedDeployment": null
    }
}

This is the JSON summarizing the user needs, field that cannot be deduced are left as null:
{
    "brand": null,
    "maxPrice"
	
==========================
The following is a JSON extracted from the user query according to this informations :
brand: a string representating the brand name of the umbrella wanted by the user.
maxPrice: a number, maximum price asked by the user in euros. Do not try to deduce a price if the user didn't explicitly gave a number.
minimumPrice: a number, minimum price asked by the user in euros.  Do not try to deduce a price if the user didn't explicitly gave a number.
approximatePrice: a string that can be only "cheap", "moderate", "expansive" or null. In this field you can try to deduce the range.
canopyColors: an array of string representating the colors of the canopy wanted by the user. It's considered to be the default color if the user doesn't specify he's talking about handle color then it's canopy color. Leave it null if no color is specified by the user.
handleColors: an array of string representating the colors of the handle wanted by the user. This is not the main color. It's alimented only if the user explicitly mention the color the handle should have.  Leave it null if no color is specified by the user.
needPrintLogo: a boolean that can be null, if the user need to have his logo printed on the canopy (true), don't need it (false) or don't mention it (null).
robustness: a number between 0 (very fragile) and 100 (very sturdy).
approximateSize: a string, can only be  "small", "medium", "large" (for 2 people), "very large" (for 3 people) or null.
exactSize: a number, the exact size required by the user, in cm.
deploymentInfos: a parent nodes with 3 sub-nodes.
approximateSpeedOfDeployment: a string, child 1 of deploymentInfos, can only be "slow", "average speed", "fast" or null.
speedOfDeployment: a number, child 2 of deploymentInfos, indicating how fast the umbrella need to be openable in seconds.
automatedDeployment: a boolean that can be null, child 3 of deploymentInfos, indicating if the user wants an umbrella that deploy easily with the push of a button (true), if he wants to open it manually (false) or if the user doesn't specify it (null).

This is the user query:
What are your 3 best selling umbrellas?

This is the JSON summarizing the user needs, field that cannot be deduced are left as null: 
{
    "brand": null,
    "maxPrice": null,
    "minimumPrice": null,
    "approximatePrice": "cheap",
    "canopyColors": ["red", "pink"],
    "handleColors": null,
    "needPrintLogo": true,
    "robustness": null,
    "approximateSize": "very large",
    "exactSize": null,
    "deploymentInfos": {
        "approximateSpeedOfDeployment": null,
        "speedOfDeployment": null,
        "automatedDeployment": null
    }
}

This is the JSON summarizing the user needs, field that cannot be deduced are left as null:
{
    "brand": null,
    "maxPrice"
	
	
===========
The following is a JSON extracted from the user query according to this informations :
brand: a string indicating the brand name of the umbrella wanted by the user. Leave empty if unspecified.
maxPrice: a number, maximum price asked by the user in euros. Do not try to deduce a price if the user didn't explicitly gave a number.
minimumPrice: a number, minimum price asked by the user in euros.  Do not try to deduce a price if the user didn't explicitly gave a number.
approximatePrice: a string that can be only "cheap", "moderate", "expansive" or null. In this field you can try to deduce the range.
canopyColors: an array of string representating the colors of the canopy wanted by the user. It's considered to be the default color if the user doesn't specify he's talking about handle color then it's canopy color. Leave it null if no color is specified by the user.
handleColors: an array of string representating the colors of the handle wanted by the user. This is not the main color. It's alimented only if the user explicitly mention the color the handle should have.  Leave it null if no color is specified by the user.
needPrintLogo: a boolean that can be null, if the user need to have his logo printed on the canopy (true), don't need it (false) or don't mention it (null).
robustness: a number between 0 (very fragile) and 100 (very sturdy).
approximateSize: a string, can only be  "small", "medium", "large" (for 2 people), "very large" (for 3 people) or null.
exactSize: a number, the exact size required by the user, in cm.
deploymentInfos: a parent nodes with 3 sub-nodes.
approximateSpeedOfDeployment: a string, child 1 of deploymentInfos, can only be "slow", "average speed", "fast" or null.
speedOfDeployment: a number, child 2 of deploymentInfos, indicating how fast the umbrella need to be openable in seconds.
automatedDeployment: a boolean that can be null, child 3 of deploymentInfos, indicating if the user wants an umbrella that deploy easily with the push of a button (true), if he wants to open it manually (false) or if the user doesn't specify it (null).

This is the user query:
cheap automated umbrella for branding

This is the JSON summarizing the user needs, field that cannot be deduced are left as null: 
{
    "brand": null,
    "maxPrice": null,
    "minimumPrice": null,
    "approximatePrice": "cheap",
    "canopyColors": null,
    "handleColors": null,
    "needPrintLogo": true,
    "robustness": null,
    "approximateSize": null,
    "exactSize": null,
    "deploymentInfos": {
        "approximateSpeedOfDeployment": null,
        "speedOfDeployment": null,
        "automatedDeployment": true
    }
}

======================
The following is a JSON extracted from the user query according to this informations :
brands: array of string indicating the umbrella brand names wanted by the user. Leave empty if unspecified.
maxPrice: a number, maximum price asked by the user in euros. Do not try to deduce a price if the user didn't explicitly gave a number.
minimumPrice: a number, minimum price asked by the user in euros.  Do not try to deduce a price if the user didn't explicitly gave a number.
approximatePrice: a string that can be only "cheap", "moderate", "expensive" or null. In this field you can try to deduce the range.
canopyColors: an array of string representating the colors of the canopy wanted by the user. It's considered to be the default color if the user doesn't specify he's talking about handle color then it's canopy color. Leave it null if no color is specified by the user.
handleColors: an array of string representating the colors of the handle wanted by the user. This is not the main color. It's alimented only if the user explicitly mention the color the handle should have.  Leave it null if no color is specified by the user.
approximateRobustness: a string, can only be  "fragile", "durable", "indestructible" or null.
customCanopyLogo: a boolean that can be null, if the user need to have his logo printed on the canopy (true), don't need it (false) or don't mention it (null).
approximateCanopySize: a string, can only be  "small", "medium", "large" (for 2 people), "very large" (for 3 people) or null.
exactCanopySize: a number, the exact size required by the user, in cm.
approximateSpeedOfDeployment: a string, can only be "slow", "average speed", "fast" or null.
speedOfDeployment: a number, indicating how fast the umbrella need to be openable in seconds.
automatedDeployment: a boolean that can be null, indicating if the user wants an umbrella that deploy easily with the push of a button (true), if he wants to open it manually (false) or if the user doesn't specify it (null).
orderResultBy: a string, indicating how the user wants to organize the results, can only be  "price", "reverse price", "speedOfDeployment", "reverse speedOfDeployment", "canopySize", "reverse canopySize" or null.

This is the user query:
cheap automated umbrella for branding

This is the JSON summarizing the user needs, field that cannot be deduced are left as null: 
{
    "
	
	
	{
    "brands": null,
    "maxPrice": null,
    "minimumPrice": null,
    "approximatePrice": null,
    "canopyColors": null,
    "handleColors": null,
    "approximateRobustness": null,
    "customCanopyLogo": null,
    "approximateCanopySize": null,
    "exactCanopySize": null,
	"foldable": null,
	"approximateSpeedOfDeployment": null,
	"speedOfDeployment": null,
    "automatedDeployment": null
	"orderResultBy": null
}